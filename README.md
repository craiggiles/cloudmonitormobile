Cloud Monitor Mobile

This project was a simple prototype to gain some exposure to Xamarin Studio 2.0
and how it integrates with my current development environment. 

This prototype connects to the watchmouse API (http://apidoc.watchmouse.com/)
and grabs a list of synthetic test data performed by the Cloud Monitor 
Reporting system on the vsp.com websites. Once the list of probe data
has been downloaded, it is converted into a UITableView and shown to the user.
