using System;
using System.Drawing;

using System.Collections.Generic;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Xml;

namespace CloudMonitorMobile {
	public partial class CloudMonitorMobileViewController : UIViewController {
		List<Probe> probes;

		public CloudMonitorMobileViewController () : base ("CloudMonitorMobileViewController", null) {
			probes = new List<Probe> ();
		}
		
		public override void DidReceiveMemoryWarning () {
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
		}
		
		public override void ViewDidLoad () {
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.

		}
		
		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation) {
			// Return true for supported orientations
			return (toInterfaceOrientation != UIInterfaceOrientation.PortraitUpsideDown);
		}

		partial void Okay_ButtonClick (NSObject sender) {
			Console.WriteLine("BUTTON CLICKED");
			String user = this.txtUsername.Text;
			String pass = this.txtPassword.Text;
			this.txtPassword.ResignFirstResponder();

			if (user.Trim().Length == 0 || pass.Trim ().Length == 0) return;
			XmlDocument doc = API.Instance().GetLogData(user, pass, "fad", "2013-01-01", "2013-01-02");

			//used for debuging
//			XmlDocument doc = new XmlDocument();
//			doc.Load ("testData.xml");

			if (doc != null) {
				populateProbes(doc);
			}
		}

		private void populateProbes(XmlDocument doc) {
			try {
				using (XmlToProbeConversion xml = new XmlToProbeConversion()) {
					this.tblProbeList.Source = new ProbeTableSource(xml.ConvertXmlToProbes(doc));
				}

				tblProbeList.ReloadData();
				Add (this.tblProbeList);
			} catch(Exception ex) {
				Console.WriteLine("Exception caught:" + ex.ToString());
				API.Instance ().logout ();
			}
		}

	}//end class
}//end namesace

