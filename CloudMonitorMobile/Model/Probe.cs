using System;
using System.Xml;
using System.Text;

namespace CloudMonitorMobile {
	public class Probe : ICloneable {
		public string Name { get; set; }
		public int Id { get; set; }
		public string Timestamp { get; set; }
		public string Description { get; set; }
		public int TransactionTime { get; set; }

		public override string ToString () {
			StringBuilder sb = new StringBuilder ();
			sb.Append ("Monitor: " + Name + "\n");
			sb.Append ("   Time: " + Timestamp + "\n");
			sb.Append ("  Error: ");
			if (Description.Trim ().Length == 0)
				sb.Append (" None\n");
			else
				sb.Append (Description + "\n");
			sb.Append ("   Time: " + TransactionTime + "\n");
			return sb.ToString ();
		}

		#region ICloneable implementation
		public object Clone () {
			Probe p = new Probe ();
			p.Name = Name;
			p.Id = Id;
			p.Timestamp = Timestamp;
			p.Description = Description;
			p.TransactionTime = TransactionTime;
			return p;
		}
		#endregion
	}//end class
}//end namespace

