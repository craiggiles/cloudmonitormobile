// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace CloudMonitorMobile
{
	[Register ("CloudMonitorMobileViewController")]
	partial class CloudMonitorMobileViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton btnOkay { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtUsername { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtPassword { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView tblProbeList { get; set; }

		[Action ("Okay_ButtonClick:")]
		partial void Okay_ButtonClick (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (btnOkay != null) {
				btnOkay.Dispose ();
				btnOkay = null;
			}

			if (txtUsername != null) {
				txtUsername.Dispose ();
				txtUsername = null;
			}

			if (txtPassword != null) {
				txtPassword.Dispose ();
				txtPassword = null;
			}

			if (tblProbeList != null) {
				tblProbeList.Dispose ();
				tblProbeList = null;
			}
		}
	}
}
