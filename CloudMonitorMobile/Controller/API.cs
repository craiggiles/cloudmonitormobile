using System;
using System.Xml;
using System.Net;
using System.IO;

namespace CloudMonitorMobile {

	/// <summary>
	/// API is a Singleton Class that provides an interface between the client
	/// and the Cloud Monitor service in order to obtain a set of probes used
	/// to determine website health.
	/// </summary>
	public class API {
		private static API ptr = null;
		private string nkey;
		
		private API () {
		}

		/// <summary>
		/// Grab the instance of the API. Only one instance will be 
		/// constructed at any given time.
		/// </summary>
		public static API Instance () {
			if (ptr == null)
				ptr = new API ();
			return ptr;
		}

		/// <summary>
		/// Logs into the Cloud Monitor 'watchmouse' api, downloads the log data for
		/// the specified monitor inside the time range, and then logs out of the
		/// 'watchmouse' system. 
		/// </summary>
		/// <returns>XmlDocument of the log data</returns>
		/// <param name="monitor">Monitor</param>
		/// <param name="start">Start Time</param>
		/// <param name="end">End Time</param>
		public XmlDocument GetLogData (String user, String pass, String monitor, String start, String end) {
			if (user.Trim ().Length == 0 || 
			    pass.Trim ().Length == 0 || 
			    monitor.Trim ().Length == 0 || 
			    start.Trim ().Length == 0 || 
			    end.Trim ().Length == 0)
				return new XmlDocument ();

			login (user, pass);
			String url = "https://api.cloudmonitor.ca.com/1.6/rule_log?nkey=" + nkey + "&name=" + monitor + "&start_date=" + start + "&end_date=" + end.ToString () + "&num=2000";
			XmlDocument data = connect (url);
			logout ();
			return data;
		}

		/// <summary>
		/// Logs into the watchmouse system and stores the private token for use
		/// with all future transactions
		/// </summary>
		/// <param name="username">Username.</param>
		/// <param name="password">Password.</param>
		private void login (string username, string password) {
			Console.WriteLine ("Logging in");
			String url = "https://api.cloudmonitor.ca.com/1.6/acct_login?user=" + username + "&password=" + password;
			XmlDocument doc = connect (url);
			XmlNode result = doc.DocumentElement.SelectSingleNode ("result").SelectSingleNode ("nkey");
			nkey = result.InnerText;
		}

		/// <summary>
		/// Logs out of the watchmouse system and clears the private token
		/// </summary>
		public void logout () {
			Console.WriteLine ("Logging out");
			String url = "https://api.cloudmonitor.ca.com/1.6/acct_logout?nkey=" + nkey;
			connect (url);
			nkey = string.Empty;
		}

		/// <summary>
		/// Connects to the specific URL and obtains the XmlDocument sent
		/// back by the watchmouse API.
		/// </summary>
		/// <param name="url">URL to make connection to</param>
		private XmlDocument connect(String url) {
			Console.WriteLine ("Connecting to " + url);
			XmlDocument doc = new XmlDocument();
			try {
				WebRequest request = HttpWebRequest.CreateDefault(new Uri(url));
				using(WebResponse response = request.GetResponse())
				using(Stream responseStream = response.GetResponseStream()) {
					doc.Load(responseStream);
				}
			} catch (Exception ex) {
				Console.WriteLine(ex);
				logout ();
			}

			return doc;
		}
	}//end class
}//end namespace

