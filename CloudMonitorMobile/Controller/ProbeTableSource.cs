using System;
using MonoTouch.UIKit;

namespace CloudMonitorMobile {
	public class ProbeTableSource : UITableViewSource {
		Probe[] items;
		string cellIdentifier = "TableCell";

		/// <summary>
		/// Instantiate the data source for the UITableView with Cloud Monitor Probes data
		/// </summary>
		/// <param name="probes">Probes.</param>
		public ProbeTableSource (Probe[] probes) {
			int counter = -1;
			items = new Probe[probes.Length];
			foreach (Probe probe in probes) 
				items [++counter] = (Probe)probe.Clone ();
		}

		/// <summary>
		/// How many rows are in the section
		/// </summary>
		/// <returns>The in section.</returns>
		/// <param name="tableview">Tableview.</param>
		/// <param name="section">Section.</param>
		public override int RowsInSection (UITableView tableview, int section) {
			return items.Length;
		}

		/// <summary>
		/// Get's the cell for a specific slot in the table view
		/// </summary>
		/// <returns>The cell.</returns>
		/// <param name="tableView">Table view.</param>
		/// <param name="indexPath">Index path.</param>
		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath) {
			UITableViewCell cell = tableView.DequeueReusableCell (cellIdentifier);
			if (cell == null) cell = new UITableViewCell (UITableViewCellStyle.Default, cellIdentifier);

			Probe p = items [indexPath.Row];
			string cellText = p.Timestamp + "  ";
			if (p.TransactionTime > 6000)
				cellText += ": > 6s";

			cell.TextLabel.Text = cellText;

			return cell;
		}

		/// <summary>
		/// Grab the probe from within the selected row of the Table view
		/// </summary>
		/// <param name="tableView">Table view.</param>
		/// <param name="indexPath">Index path.</param>
		public override void RowSelected (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath) {
			new UIAlertView ("Probe Selected", items [indexPath.Row].ToString(), null, "Done", null).Show ();
			tableView.DeselectRow (indexPath, true);
		}


	}//end class
}//end namespace

