using System;
using System.Xml;

using System.Collections.Generic;

namespace CloudMonitorMobile {
	public class XmlToProbeConversion : IDisposable {

		public XmlToProbeConversion () {
		}

		/// <summary>
		/// Converts an XmlDocument with a list of probes into an array of Probe data.
		/// Each probe in the XML document currently has the following attributes:
		/// <probe>
		///		<name>FAD</name>
		///		<rid>248252</rid>
		///		<start>2013-01-02 23:49:13</start>
		///		<end>2013-01-02 23:54:13</end>
		///		<repeat>1</repeat>
		///		<result>0</result>
		///		<type>0</type>
		///		<descr>
		///		</descr>
		///		<ttime>5347</ttime>
		///		<rtime>
		///		</rtime>
		///		<ctime>
		///		</ctime>
		///		<ptime>4894</ptime>
		///		<dtime>5347</dtime>
		///		<dsize>155647</dsize>
		///		<loc>ss</loc>
		///		<alerts>0</alerts>
		///		<ipaddr>
		///		</ipaddr>
		///		<id>2325082</id>
		///	</probe>
		/// 
		/// This class only uses a few of these attributes. 
		/// 	Name, id, start, descr and ttime
		/// <returns>The xml to probes.</returns>
		/// <param name="doc">Document.</param>
		public Probe[] ConvertXmlToProbes(XmlDocument doc) {
			List<Probe> result = new List<Probe> ();

			XmlNode probes = doc.DocumentElement.SelectSingleNode ("result").SelectSingleNode ("probes");
			foreach (XmlNode probe in probes.ChildNodes) {
				Probe tmp = new Probe();
				tmp.Name = probe["name"].InnerText;
				tmp.Id = int.Parse(probe["id"].InnerText);
				tmp.Timestamp = probe["start"].InnerText;
				tmp.Description = probe["descr"].InnerText;
				tmp.TransactionTime = int.Parse(probe["ttime"].InnerText);
				result.Add (tmp);
			}

			return result.ToArray();
		}

		#region IDisposable implementation
		public void Dispose () {
		}
		#endregion

	}//end class
}//end namespace